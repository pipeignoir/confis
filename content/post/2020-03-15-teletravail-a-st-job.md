---
title: "Télétravail à St Job"
date: 2020-03-15
---

15 du 03 
Télétravail à St Job

Aujourd’hui je suis allée marcher à St Job avec Vincent.

C’est très joli St Job et c’est pas très loin. Il faut se dire: on est à Uccle. On peut prendre le tram 92 et s’arrêter à Fort-Jaco. A Fort-Jaco il faut traverser le petit pont au-dessus des rails puis prendre à gauche. Juste après le pont il y avait trois hommes en costume qui parlaient et j’ai écouté vaguement et ils parlaient de où aller manger le soir-même puisque tous les restaurants étaient fermés.

En effet on est arrivés sur la place principale un peu plus bas et tout était fermé.
On a un peu rigolé en s’adressant aux affiches qui promettaient des spectacles à
venir, on les pointait du doigt en disant: Annulé! Annulé ! Annulé! Et ça devenait une
sorte de petite chanson et ça nous faisait rire même si on savait pas totalement
pourquoi et on était pas totalement sûrs que ça soit si drôle.

On est remontés sur la droite de la place à travers une petite rue très mignonne.
Une ambiance de village qui semblait loin de bruxelles. Des toutes petites maisons
dans des toutes petites rues, ça donne des toutes petites places de parking alors ça
veut dire des toutes petites voitures. La plupart c’était des mini, genre mini-cooper
de luxe, qui rentraient tout juste devant la maison. Il faut se dire : On est à Uccle.

On est arrivés près d’un centre de jeunes qui s’appelait L’ANTI-RIDES où un petit
groupe de gens faisaient griller des saucisses, moyenne d’âge plutôt la soixantaine
donc on s’est dit, bon bah c’est pas les gens de l’ANTI-RIDES. On a pris un petit
chemin caché derrière le centre et on est arrivé sur le plateau Avijl. Vincent
connaissait bien l’endroit parce qu’il est déjà venu plusieurs fois y dormir pendant la
wildlife bxl, il m’a montré un coin au milieu des potagers collectifs où ils avaient
dormi et où des drogués les avaient retrouvé le matin. Apparemment les gens du
potager étaient pas contents parce que les drogués avaient cassé une table alors
vincent avait réparé la table, il m’a montré la table, qui avait été réparée avec des
clous dorés. On a circulé au milieu des potagers en longeant un petit chemin, c’était
assez étendu et il y avait des potagers un peu sérieux et d’autres qui avaient l’air
plus anarchiques. Genre certains avec des nains de jardins et des petites
installations et d’autres juste des légumes. Vincent m’a dit qu’il y avait des terres
pour les habitants et des terres pour les squatteurs, c’était un mix. On est arrivés aux
abords d’un grand champs. Des femmes nous ont dépassé et elles parlaient du
coronavirus. Enfin, je m’en suis doutée parce qu’elles disaient « télétravail ». On a
répété plusieurs fois le mot télétravail en se disant c’est bizarre ce mot quand même.
Après on en a parlé un peu mais pas tant. Mais moi j’ai continué à me répéter le mot
à plusieurs reprises dans ma tête pendant l’après-midi, et je me disais wa il est
dingue ce mot quand même, télé-travail, il contient tellement TOUTE L’ALIENATION
DU MONDE. Ensuite on a vu des animaux, une ferme, des moutons, on a continué,
on s’est assis sur un banc et j’ai retiré mes chaussettes en laine tricotées par ma
copine Yvonne au motif de l’union st gilloise parce qu’elles me tenaient trop chaud et
là vincent a bu dans ma bouteille d’eau et j’ai presque crié. Vincent il croit pas au
corona. Il pense que ça existe même pas, tant qu’il l’aura pas vu de ses yeux vu.
Samedi soir mon ami Maxime disait quasi la meme chose. Ça m’a un peu énervé sur
le coup, mais là j’ai essayé de comprendre. Vincent disait qu’il lit pas les médias et
qu’il peut pas les croire. Pourquoi il croirait les médias? Et qu’il voit pas en quoi un
état peut interdire à des gens de sortir de chez eux ou instaurer un couvre-feu pour
une grippe. Moi je disais : nan mais y’a deux choses, y’a les faits et y’a les dérives,
y’a la dérive sécuritaire qui existe par ailleurs, mais les faits, ils sont là. Vincent
disait : interdire des gens de sortir de chez eux, c’est du fascisme, c’est tout. (Il
faisait référence au fait qu’en Espagne ils ont mis des militaires dans la rue pour
empêcher les gens de sortir de chez eux).

Après on a changé de sujet parce que j’avais peur qu’on se fâche et les derniers
jours j’ai commencé à me fâcher avec des amis lors de discussions sur le corona et
ça m’attriste, ce truc péremptoire qu’on peut avoir quand il s’agit de sujets comme
ça, très sérieux, et j’aurais envie dans ces cas-là qu’on sache mieux rire, ou en tout
cas être en distance, mieux voir l’absurdité des situations aussi, pas juste leur
rigidité.

Alors on a changé pour parler de relations amoureuses parce que j’avais Roland
Barthes dans mon sac (fragments d’un discours amoureux) et j’ai lu un extrait sur la
jalousie. Ça disait : Comme jaloux je souffre quatre fois : d'être exclu, d'être
agressif, d'être fou et d'être commun.

Vincent et moi on était d’accord de dire qu’on était pas des jaloux. Moi j’ai dit « c’est
pas jalousie, c’est un autre mot, c’est d’autres choses que je peux ressentir, mais
c’est pas jalousie, j’aime pas ce mot, il est trompeur ». j’ai expliqué que parfois je me
mets toute seule dans l’embarras en prétendant que je n’ai peur de rien et que je
suis prête à tout alors que ça n’est pas vrai. On a un peu reparlé de quand on avait
été ensemble il y a 10 ans. J’ai dit « tu vois, je m’étais mise dans l’embarras, j’aurais
du réussir à mieux dire, j’ai fait la fière, parce que je voulais pas casser quelque
chose qui existait. ». Vincent a dit qu’il allait animer une conversation à l’Approche,
un squat à bruxelles, ce sera la troisième discussion qu’ils organisent où ils discutent
du sentiment amoureux en groupe de parole. Apparemment pour la prochaine ils
voulaient parler de la thématique de la jalousie mais Vincent préférerait que ça soit
le confort. Après je l’ai un peu charrié en lui disant qu’il était pas assez déconstruit
comme mec hétéro cis pour animer ce genre de discussions dites féministes. Il était
un peu vexé et il a dit « c’est compliqué d’être un mec cis de nos jours », j’étais pas
d’accord avec lui, mais on est pas entrés plus que ça dans la discussion, je lui ai dis
d’écouter le podcast avec Virginie Despentes dans les couilles sur la table puis
après je sais plus, on a fait des blagues et on a mangé un fruit, chacun son fruit.
On est repartis, on a cherché la forêt mais on était du mauvais côté, on s’est
retrouvés dans un bois et des champs, c’était beau, on a continué à marcher en
parlant de l’habitat, où habiter, on se faisait le constat qu’on était tout coincés dans
nos appartements et que le confinement du corona nous faisait flipper pour ça, cette
image là, précisément, d’être chez soi tout seul, toute seule pendant des semaines,
c’était quand même une vision d’horreur. Qu’on aurait bien fuit à la campagne mais
où? Et comment on fait avec nos enfants quand les parents sont séparés ? Et s’ils
ferment les frontières?

On est arrivés devant une maison soit en travaux soit à l’abandon. Une belle maison,
des volets bleus. C’était dur de savoir si des gens y vivaient vraiment. Vincent a pris
une enveloppe publicitaire dans la boite aux lettres pour garder le nom des
habitants. En ce moment, avec « ses » étudiants en archi à St Luc ils répertorient les
lieux vides et inoccupés à Bruxelles, en se promenant dans les rues et en faisant
des recherches sur les bâtiments. Ils organisent une permanence trois fois par
semaine pour partager ce qu’ils ont trouvé avec ceux et celles intéressées.
On a cherché à inventer un mot qui pourrait remplacer l’expression « ça dépendra
de ce qui se passe avec le corona » mais on a pas trouvé.

Après on a parlé du travail, du guichet que je veux fabriquer et que peut-être je lui
demanderais de l’aide, de comment on allait faire financièrement avec les trucs qui
s’annulaient les prochains mois et des peurs qu’on avait mais aussi des fantasmes
et des trucs souterrains qui allaient surgir dans tout ça. Puis on est repartis prendre
le tram pour rentrer, il allait bientôt faire nuit.
On a refait le truc de chanter « annulé, annulé, annulé! » à l’arrêt de tram aux
affiches environnantes.

En descendant du tram il a voulu me faire la bise et ça l’a fait râler que je refuse et
même que je sursaute mais après il rigolait de derrière la fenêtre du tram. Dans le
tram il y avait peu de gens et ils baissaient tous la tête vers le sol.
Je suis descendue à Ma Campagne. J’ai pensé aux noms des endroits : St Job, Ma
Campagne.

Je suis rentrée en vélo chez moi et je me suis dit que ça allait être une drôle de
nouvelle vie qui semblait commencer, sans être bien sûre de ce que ça voulait dire
et si c’était vraiment vrai.

Je sentais bien qu’au fond j’étais mélangée entre la peur et l’excitation.

Mathilde 